def sprawdzDlaHex(zakres):

    tablicaKoncowa=[]
    i = 0
    iloscDopasowanych = 0
    znalezione= []

    for i in range(zakres):
        nastepnaHexValue = str(hex(i)).replace("0x","")
        CRCDlaIteracji = CRCDla16Bitow(nastepnaHexValue)
        if (CRCDlaIteracji == "a77c"):
            znalezione.append(str(nastepnaHexValue))
            print ("Dla iteracji : " + str(i))
            print("Znaleziono liczbe posiadającą ten sam kod CRC ")
            print("Wartość w postaci HEX " + str(nastepnaHexValue) + " KOD CRC: " + str(CRCDlaIteracji))
            print("\n")
            iloscDopasowanych += 1
        i+=1

    tablicaKoncowa.append(iloscDopasowanych)
    tablicaKoncowa.append(znalezione)
    return tablicaKoncowa

def CRCDla16Bitow(wiadomosc, bity=8):
    crc = 0xFFFF
    for i, j in zip(wiadomosc[0::2], wiadomosc[1::2]):
        crc = crc ^ int(i+j, 16)
        for bit in range(0, bity):
            if (crc&0x0001)  == 0x0001:
                crc = ((crc >> 1) ^ 0xA001)
            else:
                crc = crc >> 1
    return klasyfikacja(crc)

def klasyfikacja(crc):
    najstarszyBit = str(hex(crc >> 8)).replace("0x","")
    najmlodszyBit = str(hex(crc & 0x00FF)).replace("0x","")
    return najmlodszyBit + najstarszyBit



zakres=1000000
final = sprawdzDlaHex(zakres)
print("Dla "+ str(zakres) + " znaleziono " + str(final[0])+ " liczb HEX posiadajacych ten sam kod CRC - A77C")