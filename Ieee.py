from datetime import datetime
from distutils.core import setup
import py2exe
import idna
import os
import sys


class Ieee(object):
    def __init__(self):
        self.binCode = "11000001110110000000000000000000"
        self.binInsideCode = "10001111011111111111111100000000"
        self.binIEEE754 = "01000111011111111111111100000000"

    def decToBin(self, exponent):
        return int(bin(exponent)[2:])


    def getBinaryCode(self):
        self.binCode=""

        self.binCode = input("Please provide in binary Your code (for example 0100 0111 0111 1111 1111 1111 0000 0000) "
                            " and it count decimal value ""\n")

        self.binCode = self.binCode.replace(" ", "")
        return self.binCode

    def getInterestedConversion(self):
        conversion = input("Please provide which conversion You want : \n"
                  "[1] - From IEEE754 to Internal code \n"
                  "[2] - From Internal Code to IEEE754\n")
        return conversion

    def convertIEEE756ToInternalCode(self):
        self.binIEEE754 = "01000111011111111111111100000000"
        self.binInternal = "10001111011111111111111100000000"

        signBit = self.binCode[0]
        exponent = self.binCode[1:9]
        significandDigits = self.binCode[9:]

        exponent = int(exponent, 2) - 127
        exponentNew = str(self.decToBin(exponent))
        exponentNew = exponentNew + "0"

        if len(exponentNew) != 8:
            zerosToEightBits = 8 - len(exponentNew)
            exponentNew = str(zerosToEightBits * "0") + exponentNew

        if signBit == "0":
            signBitNew = "1"
        else:
            signBitNew = "0"

        result = signBitNew+exponentNew+significandDigits
        print ("After conversion from IEEE754 (" + str(self.binCode)+ " )to Internal Code : \n " + " Your new code is : \n")
        return print(hex(int(result, 2)))


    def convertInternalCodeToIEEE754(self):
        # self.binInternal = "10001111011111111111111100000000"

        signBit = self.binCode[0]
        exponent = self.binCode[1:8]
        significandDigits = self.binCode[9:]

        exponent = int(exponent, 2) + 127
        exponentNew = str(self.decToBin(exponent))
        if len(exponentNew) != 8:
            zerosToEightBits = 8 - len(exponentNew)
            exponentNew = str(zerosToEightBits * "0") + exponentNew

        if signBit == "0":
            signBitNew = "1"
        else:
            signBitNew = "0"

        result = signBitNew + exponentNew + significandDigits
        print ("After conversion from Internal Code (" + str(self.binCode)+ " )to IEEE754 Code : \n " + " Your new code is : \n")
        return print(hex(int(result, 2)))


    # Not useful -> It was for decimal value from Binary
    # def calculate(self):
    #     self.binCode = "10000011000100000000000000000000"
    #     signBit = self.binCode[0]
    #     exponent = self.binCode[1:9]
    #     significandDigits = self.binCode[9:]
    #
    #     exponent = int(exponent, 2) - 127
    #     # print(self.binCode)
    #
    #     # expon = '10110000000000000000000'
    #     j = -1
    #     scoreSignificandDigits = 0
    #     for i in range(len(significandDigits)):
    #         i = int(i)
    #         if significandDigits[i] == '1':
    #             # scoreExpon = 2**(j)
    #             scoreSignificandDigits = scoreSignificandDigits + 2 ** (j)
    #             j = j - 1
    #         else:
    #             j = j - 1
    #             continue
    #
    #     # self.pattern =  (((-1)**int(signBit))*(scoreSignificandDigits)*(2**exponent))
    #     self.pattern = (((-1) ** int(signBit)) * (1 + scoreSignificandDigits) * (2 ** exponent))
    #     return self.pattern

def main():
    test = Ieee()
    while(True):
        test.getBinaryCode()
        conversion = test.getInterestedConversion()
        if(conversion==str(1)):
            print(test.convertIEEE756ToInternalCode())
            return True;
        if(conversion==str(2)):
            print(test.convertInternalCodeToIEEE754())
            return True;


main()
