from datetime import datetime
import os
import shutil
import sys
import binascii
from operator import xor


class CRCCalculator(object):

    def __init__(self):
        self.test = "01 10 00 11 00 03 06 1A C4 BA D0"
        self.zeroToCrc = "0000000000000000"
        self.divisor = "1010000000000001"

    def getHexadecimalCrcInput(self):
        self.hexCrcInput = input("Provide in hexadecimal format Your CRC field" "\n")
        self.hexCrcInput = self.hexCrcInput.replace(" ", "")

    def getValueOfRepeat(self):
         repeat = input("Provide how many repeat do You want" "\n")
         return repeat

    def hexToBinary(self):
        scale = 16  ## equals to hexadecimal
        num_of_bits = 8
        self.binCrc = bin(int(self.hexCrcInput, scale))[2:].zfill(num_of_bits)
        self.binCrc = self.binCrc + self.zeroToCrc
        return self.binCrc

    def findNextOne(self,repeatX):

        tstart = datetime.now()

        for x in range(repeatX):

            length = len(self.binCrc)-16

            newArray = ""

            for i in range(length):
                # if(self.binCrc[]=="0"):
                #     continue

                    positionOne = self.binCrc.find('1')

                    if ((len(self.binCrc) - positionOne) <= 16):
                        self.result = self.binCrc
                        # return print(self.binCrc)
                        break
                    for j in range(len(self.divisor)):
                        # newArray = newArray + xor(bool(self.binCrc[i]), bool(self.divisor[j]))
                        newArray = newArray + str((int(self.binCrc[positionOne+j]))^int(self.divisor[j]))

                    newArray=newArray+self.binCrc[positionOne+16:]
                    self.binCrc = newArray
                    # print (len(newArray));
                    newArray=''
        print("Time : ")
        print((datetime.now() - tstart).total_seconds() * 1000)
        print("Value in Hex. : ")
        return print(hex(int(self.result,2)))



test = CRCCalculator()
test.getHexadecimalCrcInput()
repeatX = test.getValueOfRepeat()
test.hexToBinary()
test.findNextOne(int(repeatX))

