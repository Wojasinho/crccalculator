from datetime import datetime
from distutils.core import setup
import py2exe
class CRCCanCalculator(object):

    def __init__(self):
        self.zeroToCrc = "000000000000000"
        # self.divisorPolynomial = "1100010110011010"
        self.divisorPolynomial="100010110011001"
        # self.testMessage = "11010101010101111001010101010101011110110111011"

    def getBinaryMessageFrom(self):
        self.binCrc=""
        self.binCrc = self.binCrc.replace(" ", "")
        # self.binCrc=self.binCrc+self.zeroToCrc
        self.binCrc = input("Provide in binary format Your message ""\n")
        while len(self.binCrc)>96:
            print("You can't provide more then 96 bits massage. Please do again")
            self.binCrc = input("Provide in binary format Your message ""\n")
            # self.binCrc = self.binCrc + self.zeroToCrc

    def getValueOfRepeat(self):
        repeat = input("Provide how many repeat do You want" "\n")
        return repeat


    def calculateCRC(self, repeatX):
        self.sumAverageTime = datetime.now()-datetime.now()
        self.binCrc = self.binCrc+self.zeroToCrc

        tstart = datetime.now()
        for x in range(repeatX):
            length = len(self.binCrc) - 15
            tstartForAverage = datetime.now()

            newArray = ""
            for i in range(length):

                positionOne = self.binCrc.find('1')

                if ((len(self.binCrc) - positionOne) <= 15):
                    self.result = self.binCrc
                    # return print(self.binCrc)
                    break
                for j in range(len(self.divisorPolynomial)):

                    # newArray = newArray + xor(bool(self.binCrc[i]), bool(self.divisor[j]))
                    newArray = newArray + str((int(self.binCrc[positionOne + j])) ^ int(self.divisorPolynomial[j]))

                newArray = newArray + self.binCrc[positionOne + 15:]
                self.binCrc = newArray
                # print (len(newArray));
                newArray = ''

            timeForLoop = (datetime.now() - tstartForAverage)
            self.sumAverageTime = timeForLoop + self.sumAverageTime


        print("Total Time : ")
        print((datetime.now() - tstart))
        print("Average Time: ")
        print(self.sumAverageTime/repeatX)
        print("Value in Hex. : ")
        return print(hex(int(self.result, 2)))

test = CRCCanCalculator()
test.getBinaryMessageFrom()
repeatX = test.getValueOfRepeat()
# test.hexToBinary()
test.calculateCRC(int(repeatX))

